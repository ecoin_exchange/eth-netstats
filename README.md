Ethereum Network Stats
============
This is a visual interface for tracking ethereum network status. It uses WebSockets to receive stats from running nodes and output them through an angular interface. It is the front-end implementation for [eth-net-intelligence-api](https://github.com/cubedro/eth-net-intelligence-api).

## Howto
1. Copy the env variables from __.env.example__ to __.env__ and set up as you please.

2. Place the following certificates in __docker/data/certs__:
  - privkey.pem
  - fullchain.pem

3. Create and run the docker images and containers:
```bash
cd docker
docker-compose up -d
```

All done!
